import ejs = require("ejs");
import fs = require("fs");
const componentTemplate = require("./templates/component-template");

export const createTestFile = (
  templatePath: string,
  destFile: string,
  data: any,
  options: any
) => {
  // ejs.renderFile(templatePath, data, options, (err, parsedStr: string) => {
  //   if (err) {
  //     console.log(err);
  //   }
  //   fs.writeFileSync(destFile, parsedStr);
  // });
  fs.writeFileSync(destFile, ejs.render(componentTemplate, data, options));
};
