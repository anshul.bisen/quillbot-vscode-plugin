import * as vscode from "vscode";
import { createTestFile } from "./renderer";
var path = require("path");

const getFileAndDir = (filePath: string) => ({
  srcFileName: path.parse(filePath).name,
  srcDir: path.dirname(filePath),
});

const getTemplate = (testType: string | undefined, filePath: string) => {
  const { srcFileName, srcDir } = getFileAndDir(filePath);
  if (!testType) {
    throw new Error("testType is undefined");
  }

  let templatePath = "";
  let destFile = "";
  switch (testType.toLowerCase()) {
    case "component": {
      templatePath = "./templates/Component.ejs";
      destFile = `${srcFileName}.spec.tsx`;
      break;
    }
    case "hooks": {
      templatePath = "./templates/Component.ejs";
      destFile = `${srcFileName}.spec.ts`;
    }
    default: {
      templatePath = "./templates/Component.ejs";
      destFile = `${srcFileName}.spec.tsx`;
    }
  }

  const destFileAbsPath = `${srcDir}${path.sep}${destFile}`;

  return {
    srcDir,
    srcFileName,
    templatePath,
    destFileAbsPath,
  };
};
export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "quillbot" is now active!');

  const createTestCallBack = async () => {
    const testType = await vscode.window.showQuickPick(["Component", "Hook"]);
    if (!vscode.window.activeTextEditor) {
      return;
    }

    const { srcDir, srcFileName, templatePath, destFileAbsPath } = getTemplate(
      testType,
      vscode.window.activeTextEditor.document.fileName
    );

    console.log(
      getTemplate(testType, vscode.window.activeTextEditor.document.fileName)
    );

    const edit = new vscode.WorkspaceEdit();
    edit.createFile(vscode.Uri.parse(destFileAbsPath), {
      ignoreIfExists: true,
    });
    vscode.workspace.applyEdit(edit);

    const data = { name: srcFileName };
    createTestFile(templatePath, destFileAbsPath, data, {});
  };
  let disposable = vscode.commands.registerCommand(
    "quillbot.extensions",
    createTestCallBack
  );

  context.subscriptions.push(disposable);
}
export function deactivate() {}
