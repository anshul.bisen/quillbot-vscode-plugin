module.exports = `import React from 'react'

// @ts-ignore
import { render, screen } from 'test-utils'
import <%=name%> from './<%=name%>'

const setup = () => {
  const view = render(<<%=name%> />)

  return {
    ...view,
    //Add getters for dom elements here
  }
}

describe('<%=name%>', () => {
  it('renders the fist-test div', () => {
    // Arrange, Act
    const { container } = setup()

    // Assert
    expect(container).toBeInTheDocument()
  })
})
`;
